package com.thesis.arduinocontroller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    public static final String PREFERENCES = "PREFERENCES";
    public static final String USERNAME = "USERNAME";
    public static final String PASSWORD = "PASSWORD";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        initializeLoginCredentials();

        EditText etUsername = findViewById(R.id.etUsername);
        EditText etPassword = findViewById(R.id.etPassword);
        Button btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(v -> {
            String username = this.preferences.getString(USERNAME, null);
            String password = this.preferences.getString(PASSWORD, null);

            if (etUsername.getText().toString().equals(username) && etPassword.getText().toString().equals(password)) {
                Intent main = new Intent(this, MainActivity.class);
                startActivity(main);
            } else {
                Toast.makeText(this, "Invalid username and password.", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void initializeLoginCredentials() {
        this.preferences = this.getSharedPreferences(PREFERENCES, MODE_PRIVATE);
        String username = this.preferences.getString(USERNAME, null);
        String password = this.preferences.getString(PASSWORD, null);

        if (username == null && password == null) {
            this.editor = preferences.edit();
            this.editor.putString(USERNAME, "hac");
            this.editor.putString(PASSWORD, "hac12345!");
            this.editor.commit();
        }

    }
}