package com.thesis.arduinocontroller.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thesis.arduinocontroller.model.Report;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class ArduinoControllerReport {
    public static final String PREFERENCES = "PREFERENCES";
    public static final String REPORTS = "REPORTS";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public ArduinoControllerReport(Context context) {
        this.preferences = context.getSharedPreferences(PREFERENCES, MODE_PRIVATE);
        this.initialiazeReports();
    }

    private void initialiazeReports() {
        String reportJSONString = this.preferences.getString(REPORTS, null);
        if (reportJSONString == null) {
            Report reportOne = new Report(1, 0,  new Long(0), new Long(0));
            Report reportTwo = new Report(2, 0,  new Long(0), new Long(0));
            Report reportThree = new Report(3, 0,  new Long(0), new Long(0));
            Report reportFour = new Report(4, 0,  new Long(0), new Long(0));
            Report reportFive = new Report(5, 0,  new Long(0), new Long(0));
            Report reportSix = new Report(6, 0,  new Long(0), new Long(0));
            Report reportSeven = new Report(7, 0,  new Long(0), new Long(0));
            Report reportEight = new Report(8, 0,  new Long(0), new Long(0));

            List<Report> reportList = new ArrayList<>();
            reportList.add(reportOne);
            reportList.add(reportTwo);
            reportList.add(reportThree);
            reportList.add(reportFour);
            reportList.add(reportFive);
            reportList.add(reportSix);
            reportList.add(reportSeven);
            reportList.add(reportEight);

            this.saveReports(reportList);
        }
    }

    private void saveReports(List<Report> reportList) {
        String reports = new Gson().toJson(reportList);
        this.editor = preferences.edit();
        this.editor.putString(REPORTS, reports);
        this.editor.commit();
    }

    public List<Report> getReports() {
        String reportJSONString = this.preferences.getString(REPORTS, null);
        if (reportJSONString == null) {
            this.initialiazeReports();
        }
        Type listType = new TypeToken<ArrayList<Report>>(){}.getType();
        return new Gson().fromJson(reportJSONString, listType);
    }

    public boolean updateReportCounter(int identifier) {
        String reportJSONString = this.preferences.getString(REPORTS, null);
        Type listType = new TypeToken<ArrayList<Report>>(){}.getType();
        List<Report> reportList = new Gson().fromJson(reportJSONString, listType);

        int index = this.findIdentifierIndex(identifier);
        int counter = reportList.get(index).getCounter();
        reportList.get(index).setCounter(counter + 1);

        this.saveReports(reportList);
        return true;
    }

    public boolean updateReportTimerElapse(int identifier, long time) {
        String reportJSONString = this.preferences.getString(REPORTS, null);
        Type listType = new TypeToken<ArrayList<Report>>(){}.getType();
        List<Report> reportList = new Gson().fromJson(reportJSONString, listType);

        int index = this.findIdentifierIndex(identifier);
        long timerElapse = reportList.get(index).getTimerElapse();
        reportList.get(index).setTimerElapse(timerElapse + time);

        this.saveReports(reportList);
        return true;
    }

    public boolean updateReportTotalTimeUsed(int identifier, long time) {
        String reportJSONString = this.preferences.getString(REPORTS, null);
        Type listType = new TypeToken<ArrayList<Report>>(){}.getType();
        List<Report> reportList = new Gson().fromJson(reportJSONString, listType);

        int index = this.findIdentifierIndex(identifier);
        long timerElapse = reportList.get(index).getTotalTimeUsed();
        reportList.get(index).setTotalTimeUsed(timerElapse + time);

        this.saveReports(reportList);
        return true;
    }

    private int findIdentifierIndex(int identifier) {
        String reportJSONString = this.preferences.getString(REPORTS, null);
        Type listType = new TypeToken<ArrayList<Report>>(){}.getType();
        List<Report> reportList = new Gson().fromJson(reportJSONString, listType);

        int index = 0;
        for (Report report : reportList) {
            if (identifier == report.getIdentifier()) {
                return index;
            }
            index = index + 1;
        }
        return index;
    }
}
