package com.thesis.arduinocontroller.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thesis.arduinocontroller.model.Device;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class ArduinoControllerDevices {
    public static final String PREFERENCES = "PREFERENCES";
    public static final String DEVICES = "DEVICES";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public ArduinoControllerDevices(Context context) {
        this.preferences = context.getSharedPreferences(PREFERENCES, MODE_PRIVATE);
        this.initializeDevices();
    }

    private void initializeDevices() {
        String devicesJSONString = this.preferences.getString(DEVICES, null);
        if (devicesJSONString == null) {
            Device deviceOne = new Device(1,"DEVICE 1", "2", "1", new Long(0), false);
            Device deviceTwo = new Device(2, "DEVICE 2", "4", "3", new Long(0), false);
            Device deviceThree = new Device(3,"DEVICE 3", "6", "5", new Long(0), false);
            Device deviceFour = new Device(4,"DEVICE 4", "8", "7", new Long(0), false);
            Device deviceFive = new Device(5,"DEVICE 5", "0", "9", new Long(0), false);
            Device deviceSix = new Device(6,"DEVICE 6", "b", "a", new Long(0), false);
            Device deviceSeven = new Device(7,"DEVICE 7", "d", "c", new Long(0), false);
            Device deviceEight = new Device(8,"DEVICE 8", "f", "e", new Long(0), false);

            List<Device> deviceList = new ArrayList<>();
            deviceList.add(deviceOne);
            deviceList.add(deviceTwo);
            deviceList.add(deviceThree);
            deviceList.add(deviceFour);
            deviceList.add(deviceFive);
            deviceList.add(deviceSix);
            deviceList.add(deviceSeven);
            deviceList.add(deviceEight);

            this.saveDevices(deviceList);
        }
    }

    private void saveDevices(List<Device> deviceList) {
        String devices = new Gson().toJson(deviceList);
        this.editor = preferences.edit();
        this.editor.putString(DEVICES, devices);
        this.editor.commit();
    }

    public List<Device> getDevices() {
        String devicesJSONString = this.preferences.getString(DEVICES, null);
        if (devicesJSONString == null) {
            this.initializeDevices();
        }
        Type listType = new TypeToken<ArrayList<Device>>(){}.getType();
        return new Gson().fromJson(devicesJSONString, listType);
    }

    public boolean updateDeviceName(int identifier, String name) {
        String devicesJSONString = this.preferences.getString(DEVICES, null);
        Type listType = new TypeToken<ArrayList<Device>>(){}.getType();
        List<Device> deviceList = new Gson().fromJson(devicesJSONString, listType);

        int index = this.findIdentifierIndex(identifier);
        deviceList.get(index).setName(name);

        this.saveDevices(deviceList);
        return true;
    }

    public boolean updateDeviceStatus(int identifier, Boolean status) {
        String devicesJSONString = this.preferences.getString(DEVICES, null);
        Type listType = new TypeToken<ArrayList<Device>>(){}.getType();
        List<Device> deviceList = new Gson().fromJson(devicesJSONString, listType);

        int index = this.findIdentifierIndex(identifier);
        deviceList.get(index).setStatus(status);

        this.saveDevices(deviceList);
        return true;
    }

    private int findIdentifierIndex(int identifier) {
        String devicesJSONString = this.preferences.getString(DEVICES, null);
        Type listType = new TypeToken<ArrayList<Device>>(){}.getType();
        List<Device> deviceList = new Gson().fromJson(devicesJSONString, listType);

        int index = 0;
        for (Device device : deviceList) {
            if (identifier == device.getIdentifier()) {
                return index;
            }
            index = index + 1;
        }
        return index;
    }

    public String getDeviceNameByIdentifier(int identifier) {
        String devicesJSONString = this.preferences.getString(DEVICES, null);
        Type listType = new TypeToken<ArrayList<Device>>(){}.getType();
        List<Device> deviceList = new Gson().fromJson(devicesJSONString, listType);

        for (Device device : deviceList) {
            if (identifier == device.getIdentifier()) {
                return device.getName();
            }
        }
        return "";
    }
}
