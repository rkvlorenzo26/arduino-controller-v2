package com.thesis.arduinocontroller.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.thesis.arduinocontroller.model.Report;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "DatabaseHelper";
    private static final String DATABASE_NAME = "db_controller";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_USAGE_HISTORY = "usage_history";

    private static final String COL_ID = "id";
    private static final String COL_IDENTIFIER = "device_identifier";
    private static final String COL_COUNTER = "counter";
    private static final String COL_DURATION = "duration";
    private static final String COL_TIMESTAMP = "timestamp";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_USAGE_HISTORY + "("
                + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COL_IDENTIFIER + " INTEGER NOT NULL,"
                + COL_COUNTER + " INTEGER NOT NULL,"
                + COL_DURATION + " INTEGER NOT NULL,"
                + COL_TIMESTAMP + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP )";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_USAGE_HISTORY + ";";
        db.execSQL(sql);
        onCreate(db);
    }

    public boolean insertUsage(int identifier, int counter, int duration) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COL_IDENTIFIER, identifier);
        values.put(COL_COUNTER, counter);
        values.put(COL_DURATION, duration);

        return db.insert(TABLE_USAGE_HISTORY, null, values) != -1;
    }

    public Report getTotalUsageByIdentifier(int identifier) {
        StringBuilder query = new StringBuilder()
                .append("SELECT ")
                .append("device_identifier, ")
                .append("COALESCE(SUM(counter), 0), ")
                .append("COALESCE(SUM(duration), 0) ")
                .append("FROM usage_history ")
                .append("WHERE device_identifier = ? ");

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query.toString(), new String[]{String.valueOf(identifier)});
        Report report = new Report();

        if (c.moveToFirst()){
            report.setIdentifier(identifier);
            report.setCounter(Integer.parseInt(c.getString(1)));
            report.setTotalTimeUsed(Long.parseLong(c.getString(2)));
        } else {
            report.setIdentifier(identifier);
            report.setCounter(0);
            report.setTotalTimeUsed(new Long(0));
        }
        return report;
    }

    public Report getTotalUsageByIdentifierAndDate(int identifier, String date) {
        StringBuilder query = new StringBuilder()
                .append("SELECT ")
                .append("device_identifier, ")
                .append("COALESCE(SUM(counter), 0), ")
                .append("COALESCE(SUM(duration), 0) ")
                .append("FROM usage_history ")
                .append("WHERE device_identifier = ? AND date(timestamp) = ?");

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query.toString(), new String[]{String.valueOf(identifier), date});
        Report report = new Report();

        if (c.moveToFirst()){
            report.setIdentifier(identifier);
            report.setCounter(Integer.parseInt(c.getString(1)));
            report.setTotalTimeUsed(Long.parseLong(c.getString(2)));
        } else {
            report.setIdentifier(identifier);
            report.setCounter(0);
            report.setTotalTimeUsed(new Long(0));
        }
        return report;
    }

    public List<Report> getDailyUsageByIdentifier(int identifier) {
        StringBuilder query = new StringBuilder()
                .append("SELECT ")
                .append("device_identifier, ")
                .append("date, ")
                .append("SUM(counter) AS total_counter, ")
                .append("SUM(duration) AS total_duration ")
                .append("FROM ( ")
                .append("SELECT ")
                .append("device_identifier, ")
                .append("date(timestamp) AS date, ")
                .append("counter, ")
                .append("duration ")
                .append("FROM usage_history ")
                .append("WHERE device_identifier = ? ")
                .append(") usage ")
                .append("GROUP BY device_identifier, date ")
                .append("ORDER BY date ASC;");;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query.toString(), new String[]{String.valueOf(identifier)});
        List<Report> reportList = new ArrayList<>();

        if(c.moveToFirst())
        {
            do
            {
                Report report = new Report();
                report.setIdentifier(Integer.parseInt(c.getString(0)));
                report.setDate(c.getString(1));
                report.setCounter(Integer.parseInt(c.getString(2)));
                report.setTotalTimeUsed(Long.parseLong(c.getString(3)));
                reportList.add(report);
            } while(c.moveToNext());
        }
        c.close();
        db.close();

        return reportList;
    }


}
