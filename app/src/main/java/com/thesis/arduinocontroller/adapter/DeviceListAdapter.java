package com.thesis.arduinocontroller.adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.thesis.arduinocontroller.MainActivity;
import com.thesis.arduinocontroller.R;
import com.thesis.arduinocontroller.data.ArduinoControllerReport;
import com.thesis.arduinocontroller.model.Device;
import com.thesis.arduinocontroller.utils.InputFilterMinMax;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;
import static com.thesis.arduinocontroller.MainActivity.PREFERENCES;
import static com.thesis.arduinocontroller.MainActivity.RINGTONE;

public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.MyViewHolder>{

    private Context context;
    private List<Device> deviceList;
    private int seekBarValue = 1;
    private ArduinoControllerReport report;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView deviceName, tvTimer;
        public Button btnTimer, btnRename;
        public Switch btnSwitch;
        CountDownTimer timer;
        public MediaPlayer mpOn, mpOff, mpTimer;

        public MyViewHolder(View view) {
            super(view);
            deviceName = view.findViewById(R.id.deviceName);
            btnSwitch = view.findViewById(R.id.btnSwitch);
            btnTimer = view.findViewById(R.id.btnTimer);
            tvTimer = view.findViewById(R.id.tvTimer);
            btnRename = view.findViewById(R.id.btnRename);

            mpOn = MediaPlayer.create(context, R.raw.on);
            mpOff = MediaPlayer.create(context, R.raw.off);
            mpTimer = MediaPlayer.create(context, R.raw.timer);
        }
    }

    public DeviceListAdapter(Context context, List<Device> deviceList) {
        this.context = context;
        this.deviceList = deviceList;
        this.report = new ArduinoControllerReport(context);
    }

    @NonNull
    @Override
    public DeviceListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.device, viewGroup, false);
        return new DeviceListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceListAdapter.MyViewHolder myViewHolder, int position) {
        Device device = deviceList.get(position);
        myViewHolder.deviceName.setText(device.getName());
        myViewHolder.btnSwitch.setChecked(device.getStatus());

        // For Switch
        myViewHolder.btnSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            // If turned On
            if (isChecked) {
                ((MainActivity) context).ACDevices.updateDeviceStatus(device.getIdentifier(), true);
                myViewHolder.mpOn.start();

                // Add history of turned on using the app;
                SimpleDateFormat SDFormat = new SimpleDateFormat("h:m:s");
                ((MainActivity) context).usageTimeMap.put(device.getIdentifier(), SDFormat.format(new Date()));

                boolean result = ((MainActivity) context).sendSignal(device.getCommandOn());
                if (result) {
                    sendNotification(device.getName() + " has been turned on.", "Arduino Controller", position);
                    this.report.updateReportCounter(device.getIdentifier());
                }
            } else { // If turned Off
                ((MainActivity) context).ACDevices.updateDeviceStatus(device.getIdentifier(), false);
                myViewHolder.mpOff.start();
                boolean result = ((MainActivity) context).sendSignal(device.getCommandOff());

                try {
                    // Compute for time that the device is open;
                    if (((MainActivity) context).usageTimeMap.containsKey(device.getIdentifier())) {
                        SimpleDateFormat SDFormat = new SimpleDateFormat("h:m:s");
                        String lt1 = ((MainActivity) context).usageTimeMap.get(device.getIdentifier());
                        Date date1 = SDFormat.parse(lt1);
                        Date date2 = SDFormat.parse(SDFormat.format(new Date()));
                        long duration  = (date2.getTime() - date1.getTime()) / 1000;
                        report.updateReportTotalTimeUsed(device.getIdentifier(), duration);
                        ((MainActivity) context).usageTimeMap.remove(device.getIdentifier());

                        ((MainActivity) context).dbHelper.insertUsage(device.getIdentifier(), 1, (int) duration);
                    }
                } catch (Exception e) {
                    if (((MainActivity) context).usageTimeMap.containsKey(device.getIdentifier())) {
                        ((MainActivity) context).usageTimeMap.remove(device.getIdentifier());
                    }
                }
                if (result) {
                    sendNotification(device.getName() + " has been turned off.", "Arduino Controller", position);
                }
            }
        });

        myViewHolder.btnTimer.setOnClickListener(v -> {
            seekBarValue = 1;

            if (device.getTimer().equals(new Long(0))) {
                LayoutInflater factory = LayoutInflater.from(context);
                final View timerDialogView = factory.inflate(R.layout.dialog_timer, null);
                final AlertDialog timerDialog = new AlertDialog.Builder(context).create();
                timerDialog.setView(timerDialogView);

                Button btnSetTimer = timerDialogView.findViewById(R.id.btnSetTimer);
                EditText etHours = timerDialogView.findViewById(R.id.etHours);
                EditText etMinutes = timerDialogView.findViewById(R.id.etMinutes);
                EditText etSeconds = timerDialogView.findViewById(R.id.etSeconds);

                // Filter for time input;
                etHours.setFilters(new InputFilter[]{ new InputFilterMinMax("0", "24")});
                etMinutes.setFilters(new InputFilter[]{ new InputFilterMinMax("0", "59")});
                etSeconds.setFilters(new InputFilter[]{ new InputFilterMinMax("1", "59")});
                etHours.setText("0");
                etMinutes.setText("0");
                etSeconds.setText("1");

                btnSetTimer.setOnClickListener(v12 -> {
                    if (etHours.getText().toString().equals("") && etMinutes.getText().toString().equals("") && etSeconds.getText().toString().equals("")) {
                        Toast.makeText(context, "Please fill-up hours or minutes field.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    int hours = etHours.getText().toString().equals("") ? 0 : Integer.parseInt(etHours.getText().toString());
                    int minutes = etMinutes.getText().toString().equals("") ? 0 : Integer.parseInt(etMinutes.getText().toString());
                    int seconds = etSeconds.getText().toString().equals("") ? 0 : Integer.parseInt(etSeconds.getText().toString());
                    int timerValue = (hours * 60 * 60) + (minutes * 60) + seconds;

                    Long value = new Long(timerValue);
                    device.setTimer(value);
                    myViewHolder.btnTimer.setText("STOP");
                    myViewHolder.btnSwitch.setChecked(true);
                    myViewHolder.btnSwitch.setEnabled(false);
                    myViewHolder.btnRename.setEnabled(false);
                    timerDialog.dismiss();
                    myViewHolder.timer = new CountDownTimer(value * 1000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            report.updateReportTimerElapse(device.getIdentifier(), 1);

                            long millis = millisUntilFinished;
                            @SuppressLint("DefaultLocale")
                            String hms = String.format("%02d:%02d:%02d",
                                    (TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis))),
                                    (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis))),
                                    (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))
                            );
                            myViewHolder.tvTimer.setText("Timer: " + hms);
                        }

                        public void onFinish() {
                            report.updateReportTimerElapse(device.getIdentifier(), 1);
                            boolean result = ((MainActivity) context).sendSignal(device.getCommandOff());
                            myViewHolder.tvTimer.setText("Timer: -- : -- : --");
                            myViewHolder.btnTimer.setText("TIMER");
                            device.setTimer(new Long(0));
                            myViewHolder.btnSwitch.setEnabled(true);
                            myViewHolder.btnSwitch.setChecked(false);
                            myViewHolder.btnRename.setEnabled(true);

                            playRingtone();

                            if (result) {
                                sendNotification(device.getName() + " has been turned off.", "Arduino Controller", position);
                            }
                        }
                    }.start();
                });

                timerDialog.show();
            } else {
                if (myViewHolder.timer != null) {
                    myViewHolder.timer.cancel();
                    myViewHolder.btnSwitch.setEnabled(true);
                    myViewHolder.btnSwitch.setChecked(false);
                    myViewHolder.btnRename.setEnabled(true);
                    myViewHolder.btnTimer.setText("TIMER");
                    myViewHolder.tvTimer.setText("Timer: -- : -- : --");
                    device.setTimer(new Long(0));
                    sendNotification(device.getName() + " timer has been stopped.", "Arduino Controller", position);
                }
            }
        });
    }

    private void playRingtone() {
        SharedPreferences sharedPreferences = ((MainActivity) context).getSharedPreferences(PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor;
        String ringtone = sharedPreferences.getString(RINGTONE, "");
        final Uri deviceCurrentTone = RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_ALARM);
        if (ringtone.equals("")) {
            ringtone = deviceCurrentTone.toString();
            editor = sharedPreferences.edit();
            editor.putString(RINGTONE, ringtone);
            editor.commit();
        }
        final Uri appCurrentTone = Uri.parse(ringtone);
        Ringtone defaultRingtone = RingtoneManager.getRingtone(context, appCurrentTone);
        defaultRingtone.play();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                defaultRingtone.stop();
                t.cancel();
            }
        }, 7500);
    }

    private void sendNotification(String message, String title, int id) {
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */,
                intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_MAX);

        NotificationManager notificationManager =
                (NotificationManager) ((MainActivity) context).getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelId = "AC001";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Arduino Controller",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId(channelId);
        }

        notificationManager.notify(id, notificationBuilder.build());
    }

    @Override
    public int getItemCount() {
        return deviceList.size();
    }
}
