package com.thesis.arduinocontroller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.thesis.arduinocontroller.data.ArduinoControllerDevices;
import com.thesis.arduinocontroller.data.ArduinoControllerReport;
import com.thesis.arduinocontroller.database.DatabaseHelper;
import com.thesis.arduinocontroller.model.Report;

import java.util.List;

public class ReportActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        DatabaseHelper dbHelper = new DatabaseHelper(this);

        ArduinoControllerDevices devices = new ArduinoControllerDevices(this);
        ArduinoControllerReport report = new ArduinoControllerReport(this);
        List<Report> reportList = report.getReports();

        TableLayout tableLayout = findViewById(R.id.table_main);

        LayoutInflater headerInflater = getLayoutInflater();
        TableRow headerRow = (TableRow) headerInflater.inflate(R.layout.table_header, tableLayout, false);
        tableLayout.addView(headerRow);

        for (Report data : reportList) {
            LayoutInflater dataRowInflater = getLayoutInflater();
            TableRow dataRow = (TableRow) dataRowInflater.inflate(R.layout.table_row, tableLayout, false);
            TextView column1 = dataRow.findViewById(R.id.reportColumn1);
            TextView column2 = dataRow.findViewById(R.id.reportColumn2);
            TextView column3 = dataRow.findViewById(R.id.reportColumn3);
            TextView column4 = dataRow.findViewById(R.id.reportColumn4);

            Report result = dbHelper.getTotalUsageByIdentifier(data.getIdentifier());

            column1.setText(devices.getDeviceNameByIdentifier(data.getIdentifier()));
            column2.setText(String.valueOf(result.getCounter()));
            column3.setText(convertTimeInSecondsToTime(result.getTotalTimeUsed()));
            column4.setText(convertTimeInSecondsToTime(data.getTimerElapse()));

            tableLayout.addView(dataRow);
        }
    }

    private String convertTimeInSecondsToTime(long timeInSeconds) {
        long p1 = timeInSeconds % 60;
        long p2 = timeInSeconds / 60;
        long p3 = p2 % 60;
        p2 = p2 / 60;
        return p2 + "h " + p3 + "m " + p1 + "s";
    }
}