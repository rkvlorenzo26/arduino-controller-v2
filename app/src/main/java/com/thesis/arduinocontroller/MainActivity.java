package com.thesis.arduinocontroller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.thesis.arduinocontroller.adapter.DeviceListAdapter;
import com.thesis.arduinocontroller.data.ArduinoControllerDevices;
import com.thesis.arduinocontroller.data.ArduinoControllerReport;
import com.thesis.arduinocontroller.database.DatabaseHelper;
import com.thesis.arduinocontroller.model.Device;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    public static final String DEVICES = "DEVICES";

    public List<Device> deviceList = new ArrayList<>();
    DeviceListAdapter deviceListAdapter;
    public ArduinoControllerDevices ACDevices;
    public DatabaseHelper dbHelper;
    public ArduinoControllerReport report;

    Button connect;
    RecyclerView rvDeviceList;
    TextView tvDeviceName;
    Switch btnMainSwitch;
    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    OutputStream mmOutStream;
    String deviceHardwareAddress,TAG;

    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    private ProgressDialog progress;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    String device_address = null;
    public boolean onLoad = true;
    public Map<Integer, String> usageTimeMap;

    public static final String PREFERENCES = "PREFERENCES";
    public static final String PASSWORD = "PASSWORD";
    public static final String RINGTONE = "RINGTONE";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUI();

        usageTimeMap = new HashMap<>();
        report = new ArduinoControllerReport(this);

        connect.setOnClickListener(view -> {
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            HashMap<String, String> address = new HashMap<>();
            ArrayList<String> devices = new ArrayList<>();

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice bt : pairedDevices) {
                    devices.add(bt.getName());
                    address.put(bt.getName(), bt.getAddress());
                }
                showBluetoothDialog(address, devices);
            } else {
                Toast.makeText(MainActivity.this,"Unable to find paired devices.", Toast.LENGTH_SHORT).show();
            }
        });

        btnMainSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            progress = ProgressDialog.show(MainActivity.this, "Sending multiple commands", "Please wait!!!");
            progress.show();
            for (Device device : deviceList) {
                ACDevices.updateDeviceStatus(device.getIdentifier(), isChecked);

                if (isChecked) {
                    SimpleDateFormat SDFormat = new SimpleDateFormat("h:m:s");
                    usageTimeMap.put(device.getIdentifier(), SDFormat.format(new Date()));
                    boolean result = sendSignal(device.getCommandOn());
                    if (result) {
                        this.report.updateReportCounter(device.getIdentifier());
                    }
                } else {
                    sendSignal(device.getCommandOff());
                    try {
                        if (usageTimeMap.containsKey(device.getIdentifier())) {
                            SimpleDateFormat SDFormat = new SimpleDateFormat("h:m:s");
                            String lt1 = usageTimeMap.get(device.getIdentifier());
                            Date date1 = SDFormat.parse(lt1);
                            Date date2 = SDFormat.parse(SDFormat.format(new Date()));
                            long duration  = (date2.getTime() - date1.getTime()) / 1000;
                            report.updateReportTotalTimeUsed(device.getIdentifier(), duration);
                            usageTimeMap.remove(device.getIdentifier());

                            dbHelper.insertUsage(device.getIdentifier(), 1, (int) duration);
                        }
                    } catch (Exception e) {
                        if (usageTimeMap.containsKey(device.getIdentifier())) {
                            usageTimeMap.remove(device.getIdentifier());
                        }
                    }
                }
            }

            onLoad = false;
            this.deviceList = ACDevices.getDevices();
            deviceListAdapter = new DeviceListAdapter(this, deviceList);
            rvDeviceList.setAdapter(deviceListAdapter);
            progress.dismiss();
        });
    }

    private void initializeUI(){
        // Initialize devices;
        ACDevices = new ArduinoControllerDevices(this);
        this.deviceList = ACDevices.getDevices();

        connect = findViewById(R.id.btnConnect);
        tvDeviceName = findViewById(R.id.tvDeviceName);
        btnMainSwitch = findViewById(R.id.btnMainSwitch);

        dbHelper = new DatabaseHelper(this);

        rvDeviceList = findViewById(R.id.rvDeviceList);
        deviceListAdapter = new DeviceListAdapter(this, deviceList);
        rvDeviceList.setLayoutManager(new LinearLayoutManager(this));
        rvDeviceList.setAdapter(deviceListAdapter);

        checkMainSwitch();
    }

    public void checkMainSwitch() {
        int total = deviceList.size();
        int counter = 0;
        for (Device device : deviceList) {
            if (device.getStatus()) {
                counter = counter + 1;
            }
        }

        if (counter == total) {
            btnMainSwitch.setChecked(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_table_report:
                Intent report = new Intent(this, ReportActivity.class);
                startActivity(report);
                break;
            case R.id.action_visual_report:
                Intent visualReport = new Intent(this, VisualReportActivity.class);
                startActivity(visualReport);
                break;
            case R.id.action_change_password:
                showChangePassword();
                break;
            case R.id.action_change_alarm:
                changeAlarm();
                break;
            case R.id.action_logout:
                finish();
                break;
        }
        return(super.onOptionsItemSelected(item));
    }

    private void changeAlarm() {
        final Uri deviceCurrentTone = RingtoneManager.getActualDefaultRingtoneUri(MainActivity.this, RingtoneManager.TYPE_ALARM);
        this.preferences = this.getSharedPreferences(PREFERENCES, MODE_PRIVATE);
        String ringtone = this.preferences.getString(RINGTONE, "");
        if (ringtone.equals("")) {
            ringtone = deviceCurrentTone.toString();
            this.editor = preferences.edit();
            this.editor.putString(RINGTONE, ringtone);
            this.editor.commit();
        }

        final Uri appCurrentTone = Uri.parse(ringtone);
        Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_RINGTONE);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Alarm Tone");
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, appCurrentTone);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false);
        intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
        startActivityForResult(intent, 999);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 999 && resultCode == RESULT_OK) {
            if (null != data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI)) {
                Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                this.editor = preferences.edit();
                this.editor.putString(RINGTONE, uri.toString());
                this.editor.commit();
            }
        }
    }

    private void showChangePassword() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View changePasswordDialogView = factory.inflate(R.layout.dialog_change_password, null);
        final AlertDialog changePasswordDialog = new AlertDialog.Builder(this).create();
        changePasswordDialog.setView(changePasswordDialogView);

        Button btnChangePassword = changePasswordDialogView.findViewById(R.id.btnChangePassword);
        EditText etPassword = changePasswordDialogView.findViewById(R.id.etChangePassword);
        EditText etRePassword = changePasswordDialogView.findViewById(R.id.etReChangePassword);
        TextView tvPasswordStrength = changePasswordDialogView.findViewById(R.id.tvPasswordStrength);

        TextWatcher passwordWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                String password = etPassword.getText().toString().trim();
                Pattern letter = Pattern.compile("[a-zA-z]");
                Pattern digit = Pattern.compile("[0-9]");
                Pattern special = Pattern.compile ("[!@#$%&*()_+=|<>?{}\\[\\]~-]");

                Matcher hasLetter = letter.matcher(password);
                Matcher hasDigit = digit.matcher(password);
                Matcher hasSpecial = special.matcher(password);

                if (password.length() < 8) {
                    tvPasswordStrength.setText("Password Strength: Weak");
                } else if (password.length() >= 8) {
                    if (hasLetter.find() && hasDigit.find() && hasSpecial.find()) {
                        tvPasswordStrength.setText("Password Strength: Strong");
                    } else {
                        tvPasswordStrength.setText("Password Strength: Medium");
                    }
                }
            }
        };

        etPassword.addTextChangedListener(passwordWatcher);

        btnChangePassword.setOnClickListener(v -> {
            if (etPassword.getText().toString().trim().equals("") || etRePassword.getText().toString().trim().equals("")) {
                Toast.makeText(this, "Please fill-up all fields.", Toast.LENGTH_SHORT).show();
            } else if (!etPassword.getText().toString().trim().equals(etRePassword.getText().toString().trim())) {
                Toast.makeText(this, "Password does not match.", Toast.LENGTH_SHORT).show();
            } else {
                this.preferences = this.getSharedPreferences(PREFERENCES, MODE_PRIVATE);
                this.editor = preferences.edit();
                this.editor.putString(PASSWORD, etPassword.getText().toString());
                this.editor.commit();
                changePasswordDialog.dismiss();
            }
        });

        changePasswordDialog.show();
    }

    public void refreshList() {
        this.deviceList.clear();
        this.deviceList.addAll(ACDevices.getDevices());
        deviceListAdapter.notifyDataSetChanged();
    }

    public boolean sendSignal ( String command ) {
        if (btSocket != null) {
            try {
                btSocket.getOutputStream().write(command.toString().getBytes());
                return true;
            } catch (IOException e) {
                Toast.makeText(MainActivity.this,"Unable to send signal.",Toast.LENGTH_SHORT).show();
                return false;
            }
        } else {
            Toast.makeText(MainActivity.this,"Unable to send signal. Make sure that you are connected to the device.",Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public void showBluetoothDialog(HashMap<String, String> address, ArrayList<String> devices) {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your device");

        // add a list
        String[] names = devices.toArray(new String[address.size()]);
        builder.setItems(names, (dialog, position) -> {
            deviceHardwareAddress = address.get(names[position]);
            tvDeviceName.setText("Connected to: " + names[position] + " - " + deviceHardwareAddress);
            connect.setText("Connect");
            connect.setEnabled(false);

            device_address = deviceHardwareAddress;
            new ConnectBT().execute();
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(MainActivity.this, "Connecting...", "Please wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (btSocket == null || !isBtConnected)
                {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(device_address);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                Toast.makeText(MainActivity.this,"Connection Failed. Is it a SPP Bluetooth? Try again.",Toast.LENGTH_SHORT).show();
                finish();
            }
            else
            {
                Toast.makeText(MainActivity.this,"Connected",Toast.LENGTH_SHORT).show();
                isBtConnected = true;
            }
            progress.dismiss();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if (isBtConnected) {
            connect.setEnabled(false);
            new ConnectBT().execute();
        }
    }

    private void sendNotification(String message, String title, int id) {
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */,
                intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_MAX);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(id, notificationBuilder.build());
    }

    @Override
    public void onBackPressed() { }
}