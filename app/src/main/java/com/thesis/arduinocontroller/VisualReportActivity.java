package com.thesis.arduinocontroller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.charts.Pie;
import com.anychart.core.cartesian.series.Column;
import com.anychart.enums.Anchor;
import com.anychart.enums.HoverMode;
import com.anychart.enums.Position;
import com.anychart.enums.TooltipPositionMode;
import com.thesis.arduinocontroller.data.ArduinoControllerDevices;
import com.thesis.arduinocontroller.data.ArduinoControllerReport;
import com.thesis.arduinocontroller.database.DatabaseHelper;
import com.thesis.arduinocontroller.model.Device;
import com.thesis.arduinocontroller.model.Report;

import java.util.ArrayList;
import java.util.List;

public class VisualReportActivity extends AppCompatActivity {

    private ArduinoControllerDevices devices;
    private DatabaseHelper dbHelper;
    private Column column;
    private Cartesian cartesian;

    AnyChartView anyChartView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visual_report);

        anyChartView = findViewById(R.id.any_chart_view1);
        anyChartView.setProgressBar(findViewById(R.id.progress_bar));

        devices = new ArduinoControllerDevices(this);
        dbHelper = new DatabaseHelper(this);

        List<DataEntry> data = generateDataEntry(1, dbHelper.getDailyUsageByIdentifier(1));
        cartesian = AnyChart.column();
        column = cartesian.column(data);
//        column.tooltip().titleFormat("{%X}").position(Position.CENTER_BOTTOM).anchor(Anchor.CENTER_BOTTOM).offsetX(0d).offsetY(5d).format("{%Value}");
        column.tooltip().titleFormat("{%X}").position(Position.CENTER_BOTTOM).anchor(Anchor.CENTER_BOTTOM).offsetX(0d).offsetY(5d)
                .format("function() {" +
                        "return this.value + ' hrs'" +
                        "}");
        cartesian.animation(true);
        cartesian.title(devices.getDeviceNameByIdentifier(1) + " Usage");
        cartesian.yScale().minimum(0d);
        cartesian.yAxis(0).labels().format("{%Value}");
        cartesian.tooltip().positionMode(TooltipPositionMode.POINT);
        cartesian.interactivity().hoverMode(HoverMode.BY_X);
        cartesian.xAxis(0).title("Date");
        cartesian.yAxis(0).title("Usage");

        anyChartView.setChart(cartesian);
    }

    private void renderChart(int identifier) {
        cartesian.removeAllSeries();

        List<DataEntry> data = generateDataEntry(identifier, dbHelper.getDailyUsageByIdentifier(identifier));
        column = cartesian.column(data);
        cartesian.title(devices.getDeviceNameByIdentifier(identifier) + " Usage");
        column.tooltip().titleFormat("{%X}").position(Position.CENTER_BOTTOM).anchor(Anchor.CENTER_BOTTOM).offsetX(0d).offsetY(5d)
                .format("function() {" +
                        "return this.value + ' hrs'" +
                        "}");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.report_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        invalidateOptionsMenu();
        MenuItem report1 = menu.findItem(R.id.report1);
        MenuItem report2 = menu.findItem(R.id.report2);
        MenuItem report3 = menu.findItem(R.id.report3);
        MenuItem report4 = menu.findItem(R.id.report4);
        MenuItem report5 = menu.findItem(R.id.report5);
        MenuItem report6 = menu.findItem(R.id.report6);
        MenuItem report7 = menu.findItem(R.id.report7);
        MenuItem report8 = menu.findItem(R.id.report8);

        report1.setTitle(devices.getDeviceNameByIdentifier(1));
        report2.setTitle(devices.getDeviceNameByIdentifier(2));
        report3.setTitle(devices.getDeviceNameByIdentifier(3));
        report4.setTitle(devices.getDeviceNameByIdentifier(4));
        report5.setTitle(devices.getDeviceNameByIdentifier(5));
        report6.setTitle(devices.getDeviceNameByIdentifier(6));
        report7.setTitle(devices.getDeviceNameByIdentifier(7));
        report8.setTitle(devices.getDeviceNameByIdentifier(8));
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.report1:
                renderChart(1);
                break;
            case R.id.report2:
                renderChart(2);
                break;
            case R.id.report3:
                renderChart(3);
                break;
            case R.id.report4:
                renderChart(4);
                break;
            case R.id.report5:
                renderChart(5);
                break;
            case R.id.report6:
                renderChart(6);
                break;
            case R.id.report7:
                renderChart(7);
                break;
            case R.id.report8:
                renderChart(8);
                break;
        }
        return(super.onOptionsItemSelected(item));
    }

    private List<DataEntry> generateDataEntry(int identifier, List<Report> reportList) {
        List<DataEntry> data = new ArrayList<>();
        if (reportList.size() > 0) {
            for (Report report : reportList) {
                Report result = dbHelper.getTotalUsageByIdentifierAndDate(identifier, report.getDate());
                double val = (double) result.getTotalTimeUsed();
                data.add(new ValueDataEntry(report.getDate(), val/3600));
            }
        }
        return data;
    }
}