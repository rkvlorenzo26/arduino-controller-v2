package com.thesis.arduinocontroller.model;

public class Report {
    private int identifier;
    private int counter;
    private long totalTimeUsed;
    private long timerElapse;
    private String date;

    public Report() {}

    public Report(int identifier, int counter, long totalTimeUsed, long timerElapse) {
        this.identifier = identifier;
        this.counter = counter;
        this.totalTimeUsed = totalTimeUsed;
        this.timerElapse = timerElapse;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public long getTimerElapse() {
        return timerElapse;
    }

    public void setTimerElapse(long timerElapse) {
        this.timerElapse = timerElapse;
    }

    public long getTotalTimeUsed() {
        return totalTimeUsed;
    }

    public void setTotalTimeUsed(long totalTimeUsed) {
        this.totalTimeUsed = totalTimeUsed;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
