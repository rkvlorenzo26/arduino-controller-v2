package com.thesis.arduinocontroller.model;

public class Device {
    private int identifier;
    private String name;
    private String commandOn;
    private String commandOff;
    private Long timer;
    private Boolean status;

    public Device() {}

    public Device(int identifier, String name, String commandOn, String commandOff, Long timer, Boolean status) {
        this.identifier = identifier;
        this.name = name;
        this.commandOn = commandOn;
        this.commandOff = commandOff;
        this.timer = timer;
        this.status = status;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommandOn() {
        return commandOn;
    }

    public void setCommandOn(String commandOn) {
        this.commandOn = commandOn;
    }

    public String getCommandOff() {
        return commandOff;
    }

    public void setCommandOff(String commandOff) {
        this.commandOff = commandOff;
    }

    public Long getTimer() {
        return timer;
    }

    public void setTimer(Long timer) {
        this.timer = timer;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
